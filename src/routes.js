import { Component } from 'react'
import Provider from 'react-redux/es/components/Provider'
import model from './model'
import { Redirect, Route, Router, Switch } from 'react-router'
import StartPage from './views/StartPage'
import React from 'react'
import { createBrowserHistory } from 'history'
import Register from './views/Register'
import Login from './views/Login'
import MyInfo from './views/MyInfo'
import firebase from 'firebase'

const isAuthenticated = () => {
    return firebase.auth().currentUser !== null
}

const history = createBrowserHistory()

export default class Routes extends Component {
    render() {
        return (
            <Provider store={ model }>
                <Router history={ history }>
                    <Switch>
                        <Route exact path="/register" component={ Register }/>
                        <Route exact path="/login" component={ Login }/>
                        <PrivateRoute exact path="/myinfo" authFunction={ isAuthenticated } component={ MyInfo }/>
                        <Route path="/" component={ StartPage }/>
                    </Switch>
                </Router>
            </Provider>
        )
    }
}

const PrivateRoute = ({ component: Component, authFunction, ...rest }) => (
    <Route
        { ...rest }
        render={ props =>
            authFunction() ? (
                <Component { ...props } />
            ) : (
                <Redirect to="/login" />
            )
        }
    />
)
