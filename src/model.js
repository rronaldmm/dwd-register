import { init } from '@rematch/core'
import selectPlugin from '@rematch/select'
import models from './models'

const store = init({
    models,
    plugins: [
        selectPlugin({ sliceState: root => root })
    ]
})

export default store
