import * as firebase from 'firebase'

export function register(email, password) {
    return firebase.auth()
        .createUserWithEmailAndPassword(email, password)
}

export function signIn(email, password) {
    return firebase.auth()
        .signInWithEmailAndPassword(email, password)
}

export function signOut() {
    return firebase.auth()
        .signOut()
}

export function removeUser() {
    return firebase.auth()
        .currentUser.delete()
}

