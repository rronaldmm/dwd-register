import React, { PureComponent } from 'react'
import { removeUser, signOut } from '../firebase/authActions'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import { removeCustomer } from '../firebase/databaseActions'

class MyInfo extends PureComponent {
    render() {
        if (!this.authorized()) {
            return (
                <Redirect to="/login"/>
            )
        } else {
            return (
                <div>
                    <h1>Logged In</h1>
                    <div className="form">
                        <span>Email:</span>
                        <span>{ this.props.user.email }</span>
                        <span>Name:</span>
                        <span>{ this.props.user.name }</span>
                        <span>Company:</span>
                        <span>{ this.props.user.company }</span>
                        <span>Billing Address:</span>
                        <span>{ this.props.user.billingAddress }</span>
                    </div>
                    <button onClick={ this.handleLogout }>Log out</button>
                    <button onClick={ this.handleRemove }>remove account</button>
                </div>
            )
        }
    }

    handleLogout = async() => {
        await signOut()
        this.props.reset()
    }

    handleRemove = async() => {
        await removeUser()
        await removeCustomer({ uid: this.props.user.uid })
        //logout
        this.props.reset()
    }


    authorized() {
        return this.props.loggedIn
    }
}

const mapState = state => ({
    loggedIn: state.login.user !== null,
    user: state.login.user
})

function mapDispatchToProps(dispatch) {
    return {
        login: dispatch.login.setUser,
        reset: dispatch.login.resetUser
    }

}

export default connect(mapState, mapDispatchToProps)(MyInfo)
