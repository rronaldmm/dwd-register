import { produce } from 'immer'
import store from '../model'

export default {
    name: 'login',

    state: {
        user: null
    },

    reducers: {
        set: produce((draft, { user }) => {
            draft.user = user
        }),
        reset: produce(draft => {
            draft.user = null
        })
    },

    effects: {
        async setUser(user) {
            await store.dispatch.login.set({ user })
        },
        async resetUser() {
            await store.dispatch.login.reset()
        }
    }
}
