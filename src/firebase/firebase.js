import * as firebase from 'firebase'
import { FirebaseConfig } from './config'

firebase.initializeApp(FirebaseConfig)

export const databaseRef = firebase.database()
export const customerRef = databaseRef.ref().child('customers')
