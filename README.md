# Register Project

## Customer Wish:

I want customers to be able to register themselves on my website using an email address, secure
password, billing address, name and company identification. I want to make sure we will models the
correct data of a customer, so some validation is nice to have on their input. Our company is
complying with the new GDPR laws, so we need to safely models customer data and allow customers to
remove their own data. I don’t care about the design of the registration form, that’s something for
later concerns. As a nice to have, it would be nice to also be able to login as a customer.

### Acceptance criteria:

- Customers are able to register themselves using email+password (for login), billing address, name and company name.
- Data input is validated:
    - valid email address
    - secure password (length >= 8)
    - all data non-empty 
- Data is stored in a database
- Customers can login using email+password
- Customers can see their own provided data (except password)
- Customers can delete their account


## Project setup:

- react
- react router
- redux via rematch
- firebase

### Firebase:

To focus on the client-side functionality I chose to use the 'serverless' authentication service and database of Firebase.\
It provides the necessary elements such as registration and signing in over multiple Identity Providers, I only implemented email/password.\
Additionally, User management functionality (editing/deleting users) are added without any trouble.\
I also use the realtime database of firebase, which integrates nicely with the authentication part.

### Run project:

Console in project root:
> npm run start

## Future work:

- Layout
- Documentation / code comments
- Implement additional signin methods (google, facebook, twitter, etc.)
- Admin interface to manage users

## Sources:

- https://firebase.google.com/docs/auth/web/manage-users
- https://firebase.google.com/docs/database/web/read-and-write
- https://github.com/rematch/rematch
- https://reacttraining.com/react-router/web
- Collegial reviews:
  - Rematch version issues (rematch changed its API since my last use)
  - General review on code readability 

