import { customerRef, databaseRef } from './firebase'

export const setCustomerData = async({ uid, email, password, name, company, billingAddress }) => {
    await customerRef.child('customers/' + uid).set({
        email,
        name,
        company,
        billingAddress
    })
}

export const loadUserData = async({ uid }) => {
    return await databaseRef.ref('customers/' + uid).once('value')
}

export const removeCustomer = async({ uid }) => {
    await customerRef.ref('customers/' + uid).remove()
}

