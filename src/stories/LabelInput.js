import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { text } from '@storybook/addon-knobs/react'
import { withSmartKnobs } from 'storybook-addon-smart-knobs'
import { LabelInput } from '../components/LabelInput'
import { withInfo } from '@storybook/addon-info'
import { withKnobs } from '@storybook/addon-knobs'

LabelInput.displayName = 'LabelInput'

storiesOf('LabelInput', module)
    .addDecorator(withSmartKnobs)
    .addDecorator(withKnobs)
    .addDecorator(withInfo)
    .addParameters({
        info: {
            header: false,
            inline: true
        }
    })
    .add('auto', () => (
        <LabelInput />
    ), {
        info: {
            text: 'Text'
        }
    })
storiesOf('LabelInput', module)
    .addDecorator(withKnobs)
    .addDecorator(withInfo)
    .addParameters({
        info: {
            header: false,
            inline: true
        }
    })
    .add('text', () => (
        <LabelInput
            label={ text('label', 'label') }
            id="text"
            value={ text('value', 'value') }
            onChange={ action('changed') }
        />
    ), {
        info: {
            text: 'specific for text'
        }
    })
    .add('password', () => (
        <LabelInput
            label={ text('label') }
            id="password"
            value="password"
            type={ text('type', 'password') }
            onChange={ action('changed') }
        />
    ), {
        info: {
            text: 'password'
        }
    })