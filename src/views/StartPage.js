import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class StartPage extends Component {
    render() {
        return (
            <div>
                <h1>Home</h1>
                <ul>
                    <li><Link to="/login">Login</Link></li>
                    <li><Link to="/register">Register</Link></li>
                </ul>
            </div>
        )
    }
}


export default StartPage
