import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { text } from '@storybook/addon-knobs/react'
import { withSmartKnobs } from 'storybook-addon-smart-knobs'
import { withInfo } from '@storybook/addon-info'
import { withKnobs } from '@storybook/addon-knobs'
import { TurboButton } from '../components/TurboButton'

TurboButton.displayName = 'TurboButton'

storiesOf('TurboButton', module)
    .addDecorator(withSmartKnobs)
    .addDecorator(withKnobs)
    .addDecorator(withInfo)
    .addParameters({
        info: {
            header: false,
            inline: true
        }
    })
    .add('auto', () => (
        <TurboButton />
    ), {
        info: {
            text: 'autoKnobs'
        }
    })
storiesOf('TurboButton', module)
    .addDecorator(withKnobs)
    .addDecorator(withInfo)
    .addParameters({
        info: {
            header: false,
            inline: true
        }
    })
    .add('label', () => (
        <TurboButton
            label={ text('label', 'label') }
            onClick={ action('click') }
        />
    ), {
        info: {
            text: 'specific label'
        }
    })