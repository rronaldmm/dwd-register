import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import { setCustomerData } from '../firebase/databaseActions'
import { register } from '../firebase/authActions'
import { Redirect } from 'react-router'
import { LabelInput } from '../components/LabelInput'

class Register extends PureComponent {

    state = {
        done: false,
        email: '',
        password: '',
        password2: '',
        name: '',
        company: '',
        billingAddress: '',
        message: ''
    }


    render() {
        if (this.state.done) {
            return (
                <Redirect to="/login"/>
            )
        } else {
            return (
                <div>
                    <h1>Register:</h1>
                    <Link to="/">back</Link>
                    <div hidden={ this.state.message === '' }>{ this.state.message }</div>
                    <div className="form">
                        <LabelInput
                            label="Email"
                            id="email"
                            value={ this.state.email }
                            onChange={ this.handleChange }
                        />
                        <LabelInput
                            label="Password"
                            id="password"
                            type="password"
                            value={ this.state.password }
                            onChange={ this.handleChange }
                        />
                        <LabelInput
                            label="Repeat"
                            id="password2"
                            type="password"
                            value={ this.state.password2 }
                            onChange={ this.handleChange }
                        />
                        <LabelInput
                            label="Name"
                            id="name"
                            value={ this.state.name }
                            onChange={ this.handleChange }
                        />
                        <LabelInput
                            label="Company"
                            id="company"
                            value={ this.state.company }
                            onChange={ this.handleChange }
                        />
                        <span>Billing Address:</span>
                        <textarea
                            id="billingAddress"
                            name="billingAddress"
                            value={ this.state.billingAddress }
                            onChange={ this.handleChange }/>
                        <button onClick={ this.handleSubmit }>Register</button>
                    </div>
                </div>
            )
        }
    }

    handleSubmit = async() => {
        if (this.inputValid()) {
            try {
                const result = await register(this.state.email, this.state.password)
                await setCustomerData({
                    uid: result.user.uid,
                    email: this.state.email,
                    password: this.state.password,
                    name: this.state.name,
                    company: this.state.company,
                    billingAddress: this.state.billingAddress
                })
                this.setState({ message: '', done: true })

            } catch (error) {
                this.setState({ message: error.message })
            }

        } else {
            this.setState({ message: 'invalid input' })
        }
    }

    inputValid = () => {
        let result = true
        result &= this.state.password === this.state.password2
        result &= this.state.password.length >= 8
        result &= this.state.name.trim().length > 0
        result &= this.state.company.trim().length > 0
        result &= this.state.billingAddress.trim().length > 0
        return result
    }

    handleChange = async event => {
        await this.setState({
            [event.target.name]: event.target.value
        })
    }
}


export default Register
