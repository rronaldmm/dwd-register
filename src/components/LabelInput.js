import React, { Component, Fragment } from 'react'
import * as PropTypes from 'prop-types'

export class LabelInput extends Component {
    render() {
        return (
            <Fragment>
                <span>{ this.props.label }:</span>
                <input
                    id={ this.props.id }
                    name={ this.props.id }
                    type={ this.props.type }
                    value={ this.props.value }
                    onChange={ this.props.onChange }/>
            </Fragment>
        )
    }
}

LabelInput.propTypes = {
    /** The label to show before the input */
    label: PropTypes.string,
    /** The Id and name of the input, used for onChange()*/
    id: PropTypes.string,
    /** input type, only use text (optional) or password*/
    type: PropTypes.string,
    /** the value of this field, probably reference to (local) state */
    value: PropTypes.string,
    /** callback function when value changes */
    onChange: PropTypes.func
}

LabelInput.defaultProps = {
    label: 'label',
    type: 'text'
}