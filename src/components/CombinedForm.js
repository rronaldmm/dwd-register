import React, { Component, Fragment } from 'react'
import * as PropTypes from 'prop-types'
import { LabelInput } from './LabelInput'
import { TurboButton } from './TurboButton'

export class CombinedForm extends Component {
    render() {
        return (
            <Fragment>
                <LabelInput
                    id={ this.props.id }
                    label={ this.props.label }
                    onChange={ this.props.onChange }
                />
                <TurboButton
                    label={ this.props.buttonLabel }
                    onClick={ this.props.onClick }
                />
            </Fragment>

        )
    }
}

CombinedForm.propTypes = {
    /** The Id and name of the input, used for onChange()*/
    id: PropTypes.string,
    /** The label to show before the input */
    label: PropTypes.string,
    /** The label to show on the button */
    buttonLabel: PropTypes.string,
    /** callback function when value changes */
    onChange: PropTypes.func,
    /** callback function when button is clicked */
    onClick: PropTypes.func
}

CombinedForm.defaultProps = {
    buttonLabel: 'submit'
}