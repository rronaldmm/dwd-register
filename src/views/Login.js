import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router'
import { connect } from 'react-redux'
import { signIn } from '../firebase/authActions'
import { loadUserData } from '../firebase/databaseActions'
import { LabelInput } from '../components/LabelInput'

class Login extends PureComponent {

    state = {
        email: '',
        password: '',
        message: ''
    }

    render() {
        if (this.authorized()) {
            return (
                <Redirect to="/myInfo"/>
            )
        } else {
            return (
                <div>
                    <h1>Login:</h1>
                    <Link to="/">back</Link>
                    <div hidden={ this.state.message === '' }>{ this.state.message }</div>
                    <div className="form">
                        <LabelInput
                            label="Email"
                            id="email"
                            value={ this.state.email }
                            onChange={ this.handleChange }
                        />
                        <LabelInput
                            label="Password"
                            id="password"
                            type="password"
                            value={ this.state.password }
                            onChange={ this.handleChange }
                        />
                        <button onClick={ this.handleSubmit }>Login</button>
                    </div>
                </div>
            )
        }
    }

    authorized() {
        return this.props.loggedIn
    }

    handleSubmit = async() => {
        try {
            const result = await signIn(this.state.email, this.state.password)

            const data = await loadUserData({ uid: result.user.uid })
            const userData = data.val()
            await this.setState({ message: '' })
            this.props.login({
                uid: result.user.uid,
                email: result.user.email,
                name: userData.name,
                company: userData.company,
                billingAddress: userData.billingAddress
            })
        } catch (error) {
            this.setState({ message: error.message })
        }
    }

    handleChange = async event => {
        await this.setState({
            [event.target.name]: event.target.value
        })
    }
}

const mapState = state => ({
    loggedIn: state.login.user !== null
})

function mapDispatchToProps(dispatch) {
    return {
        login: dispatch.login.setUser
    }

}

export default connect(mapState, mapDispatchToProps)(Login)
