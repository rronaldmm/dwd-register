import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { text } from '@storybook/addon-knobs/react'
import { withSmartKnobs } from 'storybook-addon-smart-knobs'
import { withInfo } from '@storybook/addon-info'
import { withKnobs } from '@storybook/addon-knobs'
import { CombinedForm } from '../components/CombinedForm'

CombinedForm.displayName = 'CombinedForm'

storiesOf('CombinedForm', module)
    .addDecorator(withSmartKnobs)
    .addDecorator(withKnobs)
    .addDecorator(withInfo)
    .addParameters({
        info: {
            header: false,
            inline: true
        }
    })
    .add('auto', () => (
        <CombinedForm />
    ), {
        info: {
            text: 'autoKnobs'
        }
    })
storiesOf('CombinedForm', module)
    .addDecorator(withKnobs)
    .addDecorator(withInfo)
    .addParameters({
        info: {
            header: false,
            inline: true
        }
    })
    .add('label', () => (
        <CombinedForm
            label={ text('label', 'label') }
            onClick={ action('click') }
        />
    ), {
        info: {
            text: 'specific label'
        }
    })