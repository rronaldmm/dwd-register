import React, { Component } from 'react'
import * as PropTypes from 'prop-types'

export class TurboButton extends Component {
    render() {
        return (
            <button
                id={ this.props.id }
                name={ this.props.id }
                onClick={ this.props.onClick }
            >
                { this.props.label }
            </button>

        )
    }
}

TurboButton.propTypes = {
    /** The label to show before the input */
    label: PropTypes.string,
    /** The Id and name of the input, used for onChange()*/
    id: PropTypes.string,
    /** callback function when value changes */
    onClick: PropTypes.func
}

TurboButton.defaultProps = {
    label: 'submit'
}